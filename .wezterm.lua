local wezterm = require 'wezterm'
local config = wezterm.config_builder()

-- 设置颜色
config.color_scheme = 'PaleNightHC'

-- 设置字体大小
config.font_size = 15.0

-- 设置显示列数
config.initial_cols = 110

-- 设置显示行数
config.initial_rows = 25

-- 设置启动参数
config.default_gui_startup_args = {'start', '--position', 'main:21%,25%'}

-- 设置背景的不透明度
config.window_background_opacity = 0.95

-- 设置快捷键
config.keys = {
  {
    key = 'd',
    mods = 'CMD',
    action = wezterm.action.SplitHorizontal { domain = 'CurrentPaneDomain' },
  },
  {
    key = 'd',
    mods = 'CMD|SHIFT',
    action = wezterm.action.SplitVertical { domain = 'CurrentPaneDomain' },
  },

}


return config
